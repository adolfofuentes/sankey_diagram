# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 09:28:18 2020

@author: afuentesj
"""

from plotly.offline import plot
import plotly.graph_objects as go


fig = go.Figure(data=[go.Sankey(
    node = dict(
      pad = 40,
      thickness = 20,
      line = dict(color = "black", width = 0.5),
      label = ["Seven Stars", "Diamond", "Platinum", "Gold", "Silver",
               "Seven Stars", "Diamond", "Platinum", "Gold", "Silver"],
      color = ["red","salmon","orange","yellow","gray","red","salmon","orange","yellow","gray"] 
    ),
    link = dict(
      source = [0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,4,4,4,4,4], # indices correspond to labels, eg A1, A2, A2, B1, ...
      target = [5,6,7,8,9,5,6,7,8,9,5,6,7,8,9,5,6,7,8,9,5,6,7,8,9],
      value =  [72,26,0,0,1,4,80,16,1,1,0,10,74,14,1,0,1,5,93,1,0,0,0,3,97],
      color =['red','red','red','red','red',
              'salmon','salmon','salmon','salmon','salmon',
              'orange','orange','orange','orange','orange',
              'yellow','yellow','yellow','yellow','yellow',
              'gray','gray','gray','gray','gray']
  ))])

fig.update_layout(title_text="Basic Sankey Diagram", font_size=10)
plot(fig)
